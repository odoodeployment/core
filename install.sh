#!/bin/bash
set -e
set -u

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

echo
echo "
######### INSTANCE INSTALL ERP ######## #########
#						#
#                 (__)				#
#                 (oo)				#
#           /------\/				#
#          / |    ||				#
#         *  /\---/\				#
#            ~~   ~~ 				#
#						#
########## ######### ######### ######### #######"

echo
echo
echo "Welcome to this little utility program intended for the curious on erp and developer."
echo "This program allows you to install multiple instances of erp on your local machine"


###########################################################
# Variables Section 
###########################################################
re='^[0-9]+$'
#doc tab
# 0 : version name (odoo, openerp) 
# 1 : path odoo instance user (/opt/$erpUser/odoo)
# 2 : user already have odoo instance ($erpUser)
# 3 : version (7, 8, 9, 10)
#TODO : use tab is not good solution
tabVersionName[0]="version"
#write command to exe in user profile
USER_HOME=$(eval echo ~${SUDO_USER})
#browser param
sameBrowser=""
Admin_Install="0"
NOW=$(date +"%m-%d-%Y--%H--%M--%S")
array_checkdb[0]=""
cpt=0
declare -A map
database_name=""

###########################################################
# Variables Section (DB Details)
###########################################################
DB_HostName="localhost"
DB_Port="5456"
DB_Name=""
DB_UserName=""
DB_Password_Postgres=""
DB_Password_Erp=""
DB_STATUS=""
DIR_SqlFiles="$USER_HOME/sqlfiles"


##########################################################
# All Script Functions Goes Here
##########################################################

db_statuscheck() {
	echo "`date` :Checking DB connectivity...";
	echo "`date` :Trying to connect postgres database ..."
	#psql -U postgres -d $DB_Name -c "SELECT $DB_UserName FROM pg_database"
	psql postgres -c "SELECT datname FROM pg_database WHERE datistemplate = false AND datname = '$DB_Name'" &> /dev/null
	#echo $?
	#psql -h localhost -U $DB_UserName -p 5432 $DB_Name

	if [ $? -eq 0 ]
	then
		DB_STATUS="UP"
		export DB_STATUS
		echo "`date` :Status: ${DB_STATUS}. Able to Connect..."
	else
		DB_STATUS="DOWN"
		export DB_STATUS
		echo "`date` :Status: DOWN . Not able to Connect."
		echo "`date` :Not able to connect to database with Username: "${DB_UserName}" Password: "${DB_Password_Erp}" DB HostName: "${DB_HostName}" DB Port: "${DB_Port}" SID: "${DB_Name}"."
		echo "`date` :Exiting Script Run..."
		exit
	fi
	
}

runsqls() {
	if [[ "$DB_STATUS" == "DOWN" ]] ; then
		echo "`date` :DB status check failed..."
		echo "`date` :Skipping to run extra sqls and exiting..."
		exit
	else
		for file in `dir -d $DIR_SqlFiles/*` ; do
		#for file in `cat extrasqlslist.txt` ;do
			echo "`date` :Executing file $file..."
			echo "`date` :__________________________________________";
			echo "`date` :SQL OUTPUT:";
			echo "`date` :__________________________________________";
			sudo -u postgres psql template1 -f "$file"
			echo "`date` :__________________________________________";
		done
	fi
}

database_list() {
	#TODO : databases owned by this user
	#DATABASE_LIST=$(sudo -u postgres psql -l | awk '{ print $1}' | grep -vE '^-|^List|^Name|template[0|1]|postgres|\||\(|'^no_owned...)
	DATABASE_LIST=$(sudo -u postgres psql -l | awk '{ print $1}' | grep -vE '^-|^List|^Name|template[0|1]|postgres|\||\(')
	for db in $DATABASE_LIST
	do
		echo $db
		array_checkdb[$cpt]=$db
		cpt=$(($cpt+1))
	done

	if [ ! -z $DATABASE_LIST ];then
		for key in "${!array_checkdb[@]}"; do map[${array_checkdb[$key]}]="$key"; done 
	else
		echo "----NO DATABASE FOUND-----"
	fi
}
 
delete_database() {
	echo "ready to delete $database_name please wait..."
	sudo -u postgres dropdb $database_name
	echo "database $database_name deleted"
}

dump_database() {
	DIR=/opt/$erpUser/backup/psql
	[ ! -d $DIR ] && mkdir -p $DIR || :
	if [ $database_name == "ALL" ];then
		echo "ready to dump ALL databases please wait..."
		for db in $DATABASE_LIST
		do
			sudo -u postgres pg_dump $db | gzip -c >  $DIR/$NOW.$db.gz
		done
		echo "databases are dumped"
	else
		echo "ready to dump $database_name please wait..."
		sudo -u postgres pg_dump $database_name | gzip -c >  $DIR/$NOW.$database_name.gz
		echo "database $database_name is dumped"
	fi
}

check_database_in_list_and_action() {
	if [ ! -z $DATABASE_LIST ];then
		if [ -z $database_name ];then
			[[ -n "${map[$database_name]}" ]] && printf '%s\n' "$database_name is in array"  && $database_action
		else
			echo "----- NO DATABASE FOUND ------"
		fi
	fi
}


#postgres install
postgresql_version() {
	> $USER_HOME/commandrun
	if [ -d /etc/postgresql ]; then
		sudo psql -V | egrep -o '[0-9]{1,}\.[0-9]{1,}' > $USER_HOME/commandrun
	fi
}	

ungit_installed() {
	> $USER_HOME/commandrun
	sudo find /usr/ -name 'ungit*' > $USER_HOME/commandrun
}	


pgadmin_installed() {
	> $USER_HOME/commandrun
	sudo find /usr/ -name 'pgadm*' > $USER_HOME/commandrun
}

pip_cache() {
	> $USER_HOME/commandrun
	#test if pip cache ~/.bashrc			
	if grep -q 'export PIP_DOWNLOAD_CACHE*' $USER_HOME/.bashrc; then
	    echo "found"  > $USER_HOME/commandrun
	else
	    echo "notfound" > $USER_HOME/commandrun
	fi

	cmd=$(head -n 1 $USER_HOME/commandrun)
	if [ ! -z "$cmd" ];then
		if [ "$cmd" = "notfound" ];then
			echo "export PIP_DOWNLOAD_CACHE='~/.pip/cache';" >> $USER_HOME/.bashrc
		fi
		install_deps
	fi
}

configure_bashrc_virtualenv(){
	# test if virtual env is configured
	> $USER_HOME/commandrun
	if grep -q 'export WORKON_HOME*' $USER_HOME/.bashrc; then
		echo "found" > $USER_HOME/commandrun
	else
		echo "notfound" > $USER_HOME/commandrun
	fi

	cmd=$(head -n 1$USER_HOME/commandrun)
	if [ ! -z "$cmd" ];then
		if [ "$cmd" = "notfound" ];then
			echo "export WORKON_HOME=~/.virtualenvs" >> $USER_HOME/.bashrc
			echo "source /usr/local/bin/virtualenvwrapper.sh" >> $USER_HOME/.bashrc
		fi
	fi
}

install_deps() {
	#install pip && add futures downloads in cache
	sudo apt-get -y install --no-install-recommends python-setuptools python-pip supervisor nginx build-essential gcc python-dev libxml2-dev libxslt1-dev libjpeg62-dev libldap2-dev libsasl2-dev libssl-dev node-less libpq-dev libsasl2-dev libsasl2-dev curl
	sudo pip install --upgrade pip
	pip install virtualenv
	pip install virtualenvwrapper
	# configure local .bashrc for virtualenv
	# you can alson configure .profile
	configure_bashrc_virtualenv
	#install git
	sudo apt install -y git
}

run_deps_odoo_core (){
		echo "cd ~ && virtualenv venv-erpv"$erpVersion" && source venv-erpv"$erpVersion"/bin/activate && pip install -r ${tabVersionName[0]}/requirements.txt" > $USER_HOME/commandrun
		cmd=$(head -n 1 $USER_HOME/commandrun)
		sudo -H -u "$erpUser" /bin/bash -c "$cmd"
}

copy_odoo_local_core (){
	#don't clone odoo core if already exists for local user
	#read local users name
	sudo cut -d: -f1 /etc/passwd > $USER_HOME/commandrun
	for systemuserfile in $USER_HOME/commandrun; do
		#install deps
		while read systemuser; do
		  if [ -d /opt/$erpUser ];then
			if [ -d /opt/$erpUser/odoo ]; then
				if [ -f /opt/$systemuser/odoo_version.txt ]; then
					cmd=$(head -n 1 /opt/$systemuser/odoo_version.txt)
					if [ ! -z ${erpVersion+x} ]; then
						#"var is set"
						if [ $cmd == $erpVersion ]; then
							tabVersionName[1]="/opt/$systemuser/odoo"
							#user have instance odoo
							tabVersionName[2]="$systemuser"
							break
						fi
					fi
				fi 
			fi
		  fi
		done <$systemuserfile
	done
}


user_exist() {
	sudo cut -d: -f1 /etc/passwd > $USER_HOME/commandrun
	for systemuserfile in $USER_HOME/commandrun; do
		#install deps
		while read systemuser; do
		  if [ $systemuser == $erpUser ];then
			tabVersionName[2]="$systemuser"
			break
		  fi
		done <$systemuserfile
	done
}

ask_to_delete_db() {
	while true; do
		echo
	    	echo "Do you want to delete database [yes/no]"
		echo "May be there are old databases for this user"
		read resp

		case $resp in
			yes)
				echo "--------- DATABASE LIST ---------"
				database_list
				echo 
				echo "-------- enter database name -------"
				read database
				database_name=$database
				database_action=delete_database
				check_database_in_list_and_action
				;;
			no)
			     #case user already exists
			     #echo "--------- Ok ! you will assume the consequences, your erp may not work fine ---------"
			     break
			     ;;
			*)
			     echo "That is not a valid choice ! [yes/no]"
			     ;;
	       esac  
	done
}

ask_to_dump_db() {
	while true; do
		echo
	    	echo "Do you want to dump database(s) [yes/no]"
		read response

		case $response in
			yes)
				echo "--------- DATABASE LIST ---------"
				database_list
				echo 
				echo "-------- enter database name or ALL -------"
				read database
				database_name=$database
				database_action=dump_database
				if [ ! $database_name == "ALL" ];then
					check_database_in_list_and_action
				else
					dump_database
				fi
				;;
			no)
			     #case user already exists
			     #echo "--------- Ok ! you will assume the consequences, your erp may not work fine ---------"
			     break
			     ;;
			*)
			     echo "That is not a valid choice ! [yes/no]"
			     ;;
	       esac  
done
}

create_user() {
	#copy_odoo_local_core
	user_exist
	if [ ! -z ${tabVersionName[2]+x} ]; then
		#"var is set"
		echo 
		echo "This user exist do you want to delete it ? [yes] "
		read delUser
		while true; do
			if [ $delUser == "yes" ]; then
				#del user
				if [ -d /opt/$erpUser ]; then
					sudo rm -R /opt/$erpUser
				fi
				
				#dele supervisor conf
				delete_supervisor_conf

				#TODO : delete nginx config
				#create user and home (important to activate user shell)
				userdel -f $erpUser
				#TODO : delete databases user
				sudo adduser --system --home=/opt/"$erpUser" --group "$erpUser" --shell /bin/bash
				unset tabVersionName[2]
				break

				ask_to_delete_db
			else
				echo 
				echo "Bad response ! "
			fi
			echo 
			echo "You cannot have 2 users with same name"
			echo "This user exist do you want to delete it ? yes or CTRL + c to exit"
			read delUser
		done
	else
		sudo adduser --system --home=/opt/"$erpUser" --group "$erpUser" --shell /bin/bash
	fi
}

delete_erp_erp (){ 

	  echo
	  echo "Now delete repository ${tabVersionName[0]} really begins."
	  echo
	if [ -d /opt/$erpUser ];then
	 	sudo rm -R "/opt/$erpUser/${tabVersionName[0]}" 
	else
		echo "this user have not installed erp !"
	fi
}

is_wkhtmltopdf_installed (){
	command -v wkhtmltopdf >/dev/null 2>&1 || { echo >&2 "I require wkhtmltopdf but it's not installed.  Aborting."; } > $USER_HOME/commandrun
	#sudo wkhtmltopdf -V | egrep -o '[0-9]{1,}\.[0-9]{1,}' > $USER_HOME/commandrun
}

install_wkhtmltopdf (){
	#wkhtmltopdf
	if [ ! -d wkhtmltox ]; then
		sudo wget -c download.gna.org/wkhtmltopdf/0.12/0.12.3/wkhtmltox-0.12.3_linux-generic-amd64.tar.xz
		#sudo wget http://download.gna.org/wkhtmltopdf/0.12/0.12.1/wkhtmltox-0.12.1_linux-trusty-amd64.deb
		sudo tar -xvf wkhtmltox-0.12.3_linux-generic-amd64.tar.xz
		if [ ! -f /usr/bin/wkhtmltoimage ];then
			sudo mv wkhtmltox/bin/wkhtmltoimage /usr/bin/wkhtmltoimage
		fi
		if [ ! -f /usr/bin/wkhtmltopdf ];then
			sudo mv wkhtmltox/bin/wkhtmltopdf /usr/bin/wkhtmltopdf
		fi
	fi

	#clean repository
	if [ -d wkhtmltox ]; then
		sudo rm wkhtmltox-0.12.3_linux-generic-amd64.tar.xz
		sudo rm -R wkhtmltox
	fi
}

create_erp_community (){ 

	  echo
	  echo "Now create repository erp_community really begins."
	  echo

	sudo mkdir /opt/$erpUser/erp_community
	sudo chown "$erpUser":"$erpUser" /opt/$erpUser/erp_community
}
delete_erp_community (){ 

	  echo
	  echo "Now delete repository erp_community && venv-erp$erpVersion really begins."
	  echo

	 sudo rm -R "/opt/$erpUser/erp_community" 
	 sudo rm -R "/opt/$erpUser/venv-erpv$erpVersion"
}

create_addons_available_addons_enabled (){ 

	  echo
	  echo "Now create repositories addons-available && addons-enabled in erp_community really begins."
	  echo

	  clear			
	  echo "cd ~ && virtualenv venv-erpv"$erpVersion" && source venv-erpv"$erpVersion"/bin/activate && cd erp_community && mkdir addons-{available,enabled} && cd addons-available && git clone --depth 1 --branch "$erpVersion".0 --single-branch https://github.com/OCA/server-tools.git && cd ../addons-enabled && ln -s ../addons-available/server-tools/disable_openerp_online/ && ln -s ../addons-available/server-tools/cron_run_manually/" > $USER_HOME/commandrun
	cmd=$(head -n 1 $USER_HOME/commandrun)
	sudo -H -u "$erpUser" /bin/bash -c "$cmd"
}
delete_addons_available_addons_enabled (){ 

	  echo
	  echo "Now delete repositories addons-available && addons-enabled in erp_community really begins."
	  echo

	  sudo rm -R "/opt/$erpUser/erp_community/addons-available" 
	  sudo rm -R "/opt/$erpUser/erp_community/addons-enabled"
}


create_erp_dev_addons_available_addons_enabled (){ 

	  echo
	  echo "Now create repositories available && enabled in erp_dev really begins."
	  echo

	  echo "cd ~ && virtualenv venv-erpv"$erpVersion" && source venv-erpv"$erpVersion"/bin/activate && cd ~ && mkdir -p erp_dev/addons-{available,enabled}" > $USER_HOME/commandrun
	  cmd=$(head -n 1 $USER_HOME/commandrun)
	  sudo -H -u "$erpUser" /bin/bash -c "$cmd"
}
delete_erp_dev_addons_available_addons_enabled (){ 

	  echo
	  echo "Now delete repositories addons-available addons-enabled in erp_dev ."
	  echo

	sudo rm -R "/opt/$erpUser/erp_dev/addons-available" 
	sudo rm -R "/opt/$erpUser/erp_dev/addons-enabled"
}

create_erp_dev_addons (){ 

	  echo
	  echo "Now create repositories available && enabled in erp_dev really begins."
	  echo

	  echo "cd ~ && virtualenv venv-erpv"$erpVersion" && source venv-erpv"$erpVersion"/bin/activate && cd ~ && mkdir -p erp_dev" > $USER_HOME/commandrun
	  cmd=$(head -n 1 $USER_HOME/commandrun)
	  sudo -H -u "$erpUser" /bin/bash -c "$cmd"
}
delete_erp_dev_addons (){ 

	  echo
	  echo "Now delete repository erp_dev ."
	  echo

	 sudo rm -R "/opt/$erpUser/erp_dev" 
}

create_erp_server_conf (){ 

	  echo
	  echo "Now create erp-server.conf really begins."
	  echo

	if [ ! -d /opt/$erpUser/venv-erpv$erpVersion ]; then
		echo "cd ~ && virtualenv venv-erpv"$erpVersion" " > $USER_HOME/commandrun
		cmd=$(head -n 1 $USER_HOME/commandrun)
		sudo -H -u "$erpUser" /bin/bash -c "$cmd"
	fi
	clear
	echo "cd ~ && source venv-erpv"$erpVersion"/bin/activate && git clone https://gillesstephane@bitbucket.org/gillesstephane/odooconf.git" > $USER_HOME/commandrun
	cmd=$(head -n 1 $USER_HOME/commandrun)
	sudo -H -u "$erpUser" /bin/bash -c "$cmd"
	sudo -H -u "$erpUser" /bin/bash -c "cp /opt/$erpUser/odooconf/odoo-server.conf /opt/$erpUser/"
	#important
	sudo chmod a+rwx /opt/$erpUser/odoo-server.conf
}
delete_erp_server_conf (){ 

	  echo
	  echo "Now delete repository erpconf && erp-server.conf really begins."
	  echo
	#delete symb link
	sudo rm "/opt/$erpUser/odoo-server.conf"
	sudo rm -R "/opt/$erpUser/odooconf"
}

create_erp_server_log (){ 

	  echo
	  echo "Now create erp-server.log really begins."
	  echo

	echo "sudo touch /opt/$erpUser/erp-server.log" > $USER_HOME/commandrun
	cmd=$(head -n 1 $USER_HOME/commandrun)
	sudo -H -u root /bin/bash -c "$cmd"
	sudo chown "$erpUser":"$erpUser" /opt/$erpUser/erp-server.log
}
delete_erp_server_log (){ 

	  echo
	  echo "Now delete repository erp-server.log really begins."
	  echo

	sudo rm "/opt/$erpUser/erp-server.log"
}

configure_server_supervisor() {
	#TODO : see how to optimize this after
	#this is for have multiple instances
	#correction seb bug cmd before
	#copy and delete first line, put it on tmp , add new first line file and move it to supervisor
	if [[ ! -z $1 && $1 == "server" ]]; then
		sudo mv /etc/supervisor/conf.d/supervisor-odoo-server.conf /etc/supervisor/conf.d/supervisor-$erpUser-server.conf
		sudo touch /opt/$erpUser/supervisor-$erpUser-server.conf 
		sudo sed '1d' /etc/supervisor/conf.d/supervisor-$erpUser-server.conf > /opt/$erpUser/supervisor-$erpUser-server.conf
		sudo sed -i "1 i\[program\:supervisor-$erpUser-server]" /opt/$erpUser/supervisor-$erpUser-server.conf
		sudo mv /opt/$erpUser/supervisor-$erpUser-server.conf /etc/supervisor/conf.d/supervisor-$erpUser-server.conf			
	else
		#no longpolling for v7
		if [ $erpVersion != 7 ]; then
			if [[ ! -z $1 && $1 == "longpolling" ]]; then
				sudo mv /etc/supervisor/conf.d/supervisor-odoo-server-longpolling.conf /etc/supervisor/conf.d/supervisor-$erpUser-server-longpolling.conf
				#correction seb bug cmd before
				#copy and delete first line, put it on tmp , add new first line file and move it to supervisor
				sudo touch /opt/$erpUser/supervisor-$erpUser-server-$1.conf 
				sudo sed '1d' /etc/supervisor/conf.d/supervisor-$erpUser-server-$1.conf > /opt/$erpUser/supervisor-$erpUser-server-$1.conf ; 
				sudo sed -i "1 i\[program\:supervisor-$erpUser-server-$1]" /opt/$erpUser/supervisor-$erpUser-server-$1.conf
				sudo mv /opt/$erpUser/supervisor-$erpUser-server-$1.conf /etc/supervisor/conf.d/supervisor-$erpUser-server-$1.conf
			fi
		fi			
	fi
}

create_nginx (){ 

	  echo
	  echo "Now create nginx really begins."
	  echo
	clear
	sudo git clone https://gillesstephane@bitbucket.org/gmeyomesse/nginx.git /etc/nginx/sites-available/clonenginx
	sudo cp /etc/nginx/sites-available/clonenginx/nginx-xxxx.conf /etc/nginx/sites-available/nginx-$erpUser.conf
	sudo rm /etc/nginx/sites-available/clonenginx/nginx-xxxx.conf
	if [ ! -L /etc/nginx/sites-enabled/nginx-$erpUser.conf ];then
		cd /etc/nginx/sites-enabled && sudo ln -s /etc/nginx/sites-available/nginx-$erpUser.conf 
	fi
	sudo rm -R /etc/nginx/sites-available/clonenginx
}

delete_nginx () { 

	  echo
	  echo "Now delete repository nginx really begins."
	  echo
	if [ -d /etc/nginx/sites-available/clonenginx ]; then
		sudo rm -R /etc/nginx/sites-available/clonenginx
		sudo rm /etc/nginx/sites-available/nginx-$erpUser.conf
		if [ -L /etc/nginx/sites-available/nginx-$erpUser.conf ]; then
			sudo rm /etc/nginx/sites-available/erp"$erpUser".conf~
		fi
	fi

}

delete_nginx_erp_config () { 

	  echo
	  echo "Now delete repository nginx really begins."
	  echo
	if [ -d /etc/nginx/sites-available ]; then
		if [ -f /etc/nginx/sites-available/nginx-$erpUser.conf ]; then
			sudo rm /etc/nginx/sites-available/nginx-$erpUser.conf
		fi
		if [ -L /etc/nginx/sites-enabled/nginx-$erpUser.conf ]; then
			sudo rm /etc/nginx/sites-enabled/nginx-"$erpUser".conf
		fi
	fi

}


automatic_supervisor() {
	if [ ! -f /var/run/supervisor.sock ]; then
		sudo touch /var/run/supervisor.sock
		sudo chmod 777 /var/run/supervisor.sock
	fi 

	sudo service supervisor start
	sudo service supervisor restart
	sudo supervisorctl restart supervisor-$erpUser-server && sudo supervisorctl restart supervisor-$erpUser-server-longpolling
}

run_browser() {
	if [ ! -z "$sameBrowser" ]; then
		sudo -H -u `whoami` /bin/bash -c "firefox $sameBrowser localhost:$proxyPort &"
	else
		sudo -H -u `whoami` /bin/bash -c "firefox localhost:$proxyPort &"
	fi
}

run_ungit() {
	# run ungit in new term
	#run ungit on special dev repository
	source ~/.bashrc
	clear
	while true; do
		if [ ! -s $USER_HOME/commandrun ]
		then
			sleep 1
		else
			#default create sample git dev directory (for ungit demo)
			sudo mkdir /opt/$erpUser/erp_dev/addons-available/sample 
			sudo git init /opt/$erpUser/erp_dev/addons-available/sample 
			sudo chown -R $erpUser:$erpUser /opt/$erpUser/
			break
		fi
	done
	automatic_supervisor
	#try
	{				
		sudo -H -u `whoami` /bin/bash -c "cd /opt/$erpUser/erp_dev/addons-available/sample && gnome-terminal -e ungit"
		sameBrowser=""
	} || {
		echo "cannot run ungit"
	}
	
	#try...catch
	{
		sameBrowser="-new-window"
		run_browser
	} || {
		echo "cannot run firefox"
	}
	exit
}


copy_content_to_bashrc() {
	> $USER_HOME/commandrun
	#test if function exit in ~/.bashrc			
	if grep -q 'which_*' $USER_HOME/.bashrc; then
	    echo "found"  > $USER_HOME/commandrun
	else
	    echo "notfound" > $USER_HOME/commandrun
	fi

	cmd=$(head -n 1 $USER_HOME/commandrun)
	if [ ! -z "$cmd" ];then
		if [ "$cmd" = "notfound" ];then
			sudo mkdir /opt/$erpUser/which_term
			if [ -d /opt/$erpUser/which_term ]; then
				rep=/opt/$erpUser/which_term
				clear
				sudo git clone https://gmeyomesse@bitbucket.org/odoodeployment/whichterm.git $rep
			    	value=$(<$rep/which_term)
				sudo echo "$value" >> $USER_HOME/.bashrc
				#remove clone repo
				sudo rm -R $rep/which_term
			fi
		fi	
	fi
}


clone_git_remote() {
	while true; do
	    	echo "Do you want to clone somme repositories on github [1 = yes / 0 = no]:"
		read remotecloned

		case $remotecloned in
			1)
				echo "What is local repository name:"
				read localrepo
				if [ ! -z $localrepo ]; then
					#clear file content
					> $USER_HOME/commandrun
					echo "Enter git repository address "
					read gitremote
					if [ ! -z $gitremote ]; then
						#check remote exist
						sudo git ls-remote $gitremote > $USER_HOME/commandrun
						if [ -s $USER_HOME/commandrun ]; then
							sudo mkdir /opt/$erpUser/erp_dev/addons-available/$localrepo
							clear
							sudo git clone $gitremote /opt/$erpUser/erp_dev/addons-available/$localrepo
							sudo chown -R $erpUser:$erpUser /opt/$erpUser/erp_dev/addons-available/$localrepo

							if [ "$(ls -A /opt/$erpUser/erp_dev/addons-available/$localrepo)" ]; then
								echo "*** your remote has been cloned on /opt/$erpUser/erp_dev/addons-available/$localrepo"
								clear
								echo "Do yo want to see content ?"
								read response
								if ([ $response == "y" ] || [ "$response" == "Y" ] || [ "$response" == "yes" ] || [ "$response" == "Yes" ] || [ "$response" == "YES" ]); then
									sudo ls -ail /opt/$erpUser/erp_dev/addons-available/$localrepo
								fi
							else
							   echo "*** WARNING : GIT cannot clone the repository ! try another"
							fi
						fi
					fi
				fi
				;;
			0)
			     echo "OK, that is your choice no repository cloned !"
			     break
			     ;;
			*)
			     echo "That is not a valid choice ! try a number from 0 to 1."
			     ;;
	       esac  
	done
}

data_config (){
	echo 
	if [ $erpVersion == 10 ]; then
		echo -e "\n\n*************** TEST *******************\n== For test your install you can also run those cmd ==\n $ sudo su - $erpUser -s /bin/bash\n $ source venv-erpv$erpVersion/bin/activate\n $ python odoo/odoo-bin -c odoo-server.conf\n " >> /opt/$erpUser/"$erpUser"_user_configuration
	else
		echo -e "\n\n*************** TEST *******************\n== For test your install you can also run those cmd ==\n $ sudo su - $erpUser -s /bin/bash\n $ source venv-erpv$erpVersion/bin/activate\n $ python odoo/openerp-server -c odoo-server.conf\n " >> /opt/$erpUser/"$erpUser"_user_configuration
	fi
	echo -e "\n\n*************** Add $erpUser user password *******************\n == Don't forget to add user password ==\n By default system user don't have password \n command to run for add password \n  $ sudo su\n  # passwd $erpUser\n  # enter password\n  # exit\n $ su - $erpUser " >> /opt/$erpUser/"$erpUser"_user_configuration
	

	echo -e "\n\n********** For Dev module (pycharm) *************\n  == for dev give $erpUser rights access (NB :that is not good way) ==\n $ sudo chmod -R a+rwx /opt/$erpUser/" >> /opt/$erpUser/"$erpUser"_user_configuration
}

configure_git() {
	while true; do
		echo 'Configure your git [yes/no]'
		read response
		case $response in
			'yes')
				echo 'your email address'
				read responsemail
				echo 'your name commit'
				read responsename
				if [ ! -z $responsename ];then
					sudo git config --global user.name "$responsename"
				fi
				if [ ! -z $responsemail ];then
					sudo git config --global user.email "$responsemail"
				fi
				break
				;;
			'no')
				break
				;;
			 *)
				echo 'bad response !'
				;;
			esac  
		done
}

delete_supervisor_conf() {
	#delete supervisor files
	if [ -f /etc/supervisor/conf.d/supervisor-$erpUser-server.conf ]; then
		sudo rm /etc/supervisor/conf.d/supervisor-$erpUser-server.conf  
	fi
	if [ -f /etc/supervisor/conf.d/supervisor-$erpUser-server-longpolling.conf ]; then
		sudo rm /etc/supervisor/conf.d/supervisor-$erpUser-server-longpolling.conf
	fi
}

delete_erp_instance() {
	echo "what is instance name to delete ? [ex : bidon]"
	read erpUser
	if [ ! -z "$erpUser" ]; then
		#delete core directory if exists
		if [ -d /opt/$erpUser ];then		
			sudo find /opt/ -type d -name "$erpUser" > commandrun
			cmd=$(head -n 1 $USER_HOME/commandrun)
			if [ ! -z "cmd" ]; then
				sudo rm -R /opt/"$erpUser"
			fi

			#delete supervisor
			delete_supervisor_conf

			#delete nginx config
			delete_nginx_erp_config

			userdel -f $erpUser

			ask_to_delete_db
		else
			echo "This user don't have erp installed"
		fi
	fi
}
###########################################################
# program
###########################################################
echo
echo "Do you want to continue [yes/no]:"
read response
while true; do
	if ([ $response == "y" ] || [ "$response" == "Y" ] || [ "$response" == "yes" ] || [ "$response" == "Yes" ] || [ "$response" == "YES" ]); then
		echo	
		echo "let's go !"
		echo

		#delete old instances
		while true; do
			echo 'delete old erp instances ? [yes/no]'
			read responsedelete
			case "$responsedelete" in
				'yes')
					delete_erp_instance
					;;
				'no')
					echo "you gonna to install neuw instance :)"
					break
					;;
				*)
					echo "Bad response !"
					;;			
			esac
		done

		touch $USER_HOME/commandrun
		sudo chmod a+rwx $USER_HOME/commandrun
		#TODO : sed don't work on [program:supervisor-oneplus-server]

		#update system repository before start
		sudo apt-get update

		postgresql_version
		#install postgresql	
		if [ ! -s  $USER_HOME/commandrun ]; then
			sudo apt-get -y install --no-install-recommends postgresql postgresql-contrib
		fi

		#install type
		echo 
		while true; do
			echo
		    	echo "What is your level install [ 0 = simple | 1 = developper | 2 = exit ]"
			echo
			echo " 0 and press Enter for simple install"
			echo " 1 and press Enter for developper install"
			echo " 2 and press Enter to exit program"
			echo
			read response

			case $response in
				1)
					Admin_Install="dev"
					sudo apt-get -y install --no-install-recommends git
					configure_git
					break
					;;
				0)
				     Admin_Install="simple"
				     break
				     ;;
				2)
					echo "good bye have a nice day"
					exit
					;;
				*)
				     echo "That is not a valid choice ! try a number from 0 to 1."
				     ;;
		       esac  
		done

		echo 
		echo "What is your company name [eg: bidon]:"
		read erpUser
	
		DB_UserName=$erpUser

		#test user exists
		create_user
		if [ -z ${tabVersionName[2]+x} ]; then 
			pip_cache
			copy_odoo_local_core
			

			create_erp_erp (){ 

				  echo
				  echo "Now create repository erp begins."
				  echo
		
				if [ $erpVersion == "7" ]; then
					echo
					echo "---- For this version we install global python librairies ----"
					tabVersionName[0]="openerpv7"
					#get dependencies
					if [ ! -d /opt/$erpUser/openerpdeps ];then
						#TODO:adapt for copy_odoo_local_core()
						clear
						echo "cd /opt/$erpUser && git clone https://gillesstephane@bitbucket.org/gillesstephane/openerpdeps.git" > $USER_HOME/commandrun
						cmd=$(head -n 1 $USER_HOME/commandrun)
						sudo -H -u root /bin/bash -c "$cmd"
					fi
					#read files name
					dir="/opt/$erpUser/openerpdeps"
					for requirement in $dir/depsopenrep.txt; do
						echo $requirement
						#install deps
						while read dep; do
						  	echo $dep
							echo "apt-get install -y $dep" > $USER_HOME/commandrun
							cmd=$(head -n 1 $USER_HOME/commandrun)
							sudo -H -u root /bin/bash -c "$cmd"
						done <$requirement
					done

					#remove deps repository
					if [ ! -d /opt/$erpUser/openerpdeps ];then
						sudo rm -R /opt/$erpUser/openerpdeps
					fi

					#get erp deb
					if [ ! -f "/opt/$erpUser/openerp_7.0.latest.tar.gz" ];then
						sudo mkdir /opt/$erpUser/${tabVersionName[0]}
						sudo mkdir /opt/$erpUser/${tabVersionName[0]}/tmp
						sudo wget -c http://nightly.odoo.com/7.0/nightly/deb/openerp_7.0.latest.tar.gz -P /opt/$erpUser/${tabVersionName[0]}/tmp/
						sudo tar -xvf /opt/$erpUser/${tabVersionName[0]}/tmp/openerp_7.0.latest.tar.gz -C /opt/$erpUser/${tabVersionName[0]}/tmp/
						sudo cp -R /opt/$erpUser/${tabVersionName[0]}/tmp/odoo*/* /opt/$erpUser/${tabVersionName[0]}
						sudo rm -R /opt/$erpUser/${tabVersionName[0]}/tmp
						sudo touch /opt/$erpUser/odoo_version.txt
						sudo echo "$erpVersion" > /opt/$erpUser/odoo_version.txt
						sudo chown -R $erpUser:$erpUser /opt/$erpUser/${tabVersionName[0]}
						#sudo rm openerp_7.0.latest.tar.gz
					fi

				else
					tabVersionName[0]="odoo"
					if [ ! -d /opt/$erpUser/${tabVersionName[0]} ]; then
						sudo mkdir /opt/$erpUser/${tabVersionName[0]}
						sudo chown $erpUser:$erpUser /opt/$erpUser/${tabVersionName[0]}
					fi
					copy_odoo_local_core
					if [ ${#tabVersionName[@]} -gt 1 ]; then
						clear
						echo "---- You have local user with erp v$erpVersion ----"
						echo "local copy start..."
						sudo cp -R ${tabVersionName[1]} /opt/$erpUser/
						echo "local copy finish..."
						run_deps_odoo_core
					else
						clear
						echo "cd ~ && virtualenv venv-erpv"$erpVersion" && source venv-erpv"$erpVersion"/bin/activate && git clone https://www.github.com/${tabVersionName[0]}/${tabVersionName[0]} --depth 1 --branch "$erpVersion".0 --single-branch && pip install -r ${tabVersionName[0]}/requirements.txt" > $USER_HOME/commandrun
						cmd=$(head -n 1 $USER_HOME/commandrun)
						sudo -H -u "$erpUser" /bin/bash -c "$cmd"
						#odoo version on file (useful on next clone became local copy)
						sudo touch /opt/$erpUser/odoo_version.txt
						sudo echo "$erpVersion" > /opt/$erpUser/odoo_version.txt
					fi
				
				fi
			}
		
			echo
			echo "OpenERP version available to install [7,8,9 or 10]"
			echo "What version would you like to install [eg : 8]:"
			read erpVersion
			while true; do
				if ([ $erpVersion == "7" ] || [ $erpVersion == "8" ] || [ $erpVersion == "9" ] || [ $erpVersion == "10" ] && [[ $erpVersion =~ $re ]]); then
					#test installed
					is_wkhtmltopdf_installed
					if [ ! -s $USER_HOME/commandrun ]; then
						install_wkhtmltopdf
					fi


					if [ ! -d "/opt/$erpUser" ]; then
						sudo mkdir /opt/$erpUser
						sudo chown $erpUser:$erpUser /opt/$erpUser
					fi

		
					#download erp 
					if [ ! -d "/opt/$erpUser/${tabVersionName[0]}" ]; then
						create_erp_erp
					else
						echo "Delete old repository ${tabVersionName[0]} ? [y/n]"
						read deleteerperp
						if ([ $deleteerperp == "y" ] || [ "$deleteerperp" == "Y" ] || [ "$deleteerperp" == "yes" ] || [ "$deleteerperp" == "Yes" ] || [ "$deleteerperp" == "YES" ]); then
							delete_erp_erp
							create_erp_erp
						fi		
					fi

					echo "create erp community repository or delete it"
			
					#create virtual python env
					#install erp and deps
					#ps : For Ubuntu 14.10, replace virtualenv venv-erpv9 par python /usr/lib/python2.7/dist-packages/virtualenv.py venv-erpv9
					#test file erp/ erp_community/ erp_dev/ venv-erpv/ venv-erpv8/  
					if [ ! -d "/opt/$erpUser/erp_community" ]; then
						create_erp_community
					else
						echo "Delete old repository erp_community ? [y/n]"
						read deleteerpCommunity
						if ([ $deleteerpCommunity == "y" ] || [ "$deleteerpCommunity" == "Y" ] || [ "$deleteerpCommunity" == "yes" ] || [ "$deleteerpCommunity" == "Yes" ] || [ "$deleteerpCommunity" == "YES" ]); then
							delete_erp_community
							create_erp_community
						fi		
					fi
		
		
					echo "create repositories for other modules or delete it"
			
					#create repositories for other modules (addons-available && addons-enabled in erp_community )
					if [ ! -d "/opt/$erpUser/erp_community/addons-available" ]; then
						create_addons_available_addons_enabled
					else
						echo "Delete old repositories addons-available && addons_enabled ? [y/n]"
						read deleteOldaddonsAvailableaddonsEnabled
						if ([ $deleteOldaddonsAvailableaddonsEnabled == "y" ] || [ "$deleteOldaddonsAvailableaddonsEnabled" == "Y" ] || [ "$deleteOldaddonsAvailableaddonsEnabled" == "yes" ] || [ "$deleteOldaddonsAvailableaddonsEnabled" == "Yes" ] || [ "$deleteOldaddonsAvailableaddonsEnabled" == "YES" ]); then
							delete_addons_available_addons_enabled
							create_addons_available_addons_enabled
						fi
					fi

					echo "create repository for dev modules"
			
					#create repository for dev
					if [ ! -d "/opt/$erpUser/erp_dev" ]; then
						create_erp_dev_addons
					else
						echo "Delete old repository addons erp_dev ? [y/n]"
						read deleteOldaddonsDev
						if ([ $deleteOldaddonsDev == "y" ] || [ "$deleteOldaddonsDev" == "Y" ] || [ "$deleteOldaddonsDev" == "yes" ] || [ "$deleteOldaddonsDev" == "Yes" ] || [ "$deleteOldaddonsDev" == "YES" ]); then
							create_erp_dev_addons
							create_erp_dev_addons
						fi
					fi		

					echo "create repositories in dev modules"
			
					#create repository for dev
					#test files 
					if [ ! -d "/opt/$erpUser/erp_dev/addons-available" ]; then
						create_erp_dev_addons_available_addons_enabled
					else
						echo "Delete old repositories addons-available && addons-enabled in erp_dev ? [y/n]"
						read deleteOldaddonsAvailableaddonsEnabledDev
						if ([ $deleteOldaddonsAvailableaddonsEnabledDev == "y" ] || [ "$deleteOldaddonsAvailableaddonsEnabledDev" == "Y" ] || [ "$deleteOldaddonsAvailableaddonsEnabledDev" == "yes" ] || [ "$deleteOldaddonsAvailableaddonsEnabledDev" == "Yes" ] || [ "$deleteOldaddonsAvailableaddonsEnabledDev" == "YES" ]); then
							delete_erp_dev_addons_available_addons_enabled
							create_erp_dev_addons_available_addons_enabled
						fi
					fi
					echo 
					echo "==> What is your database name [eg : $erpUser""db]"
					read DB_Name
					echo 
					echo "==> What is password erp uer [eg : $erpUser""pwd]"
					read DB_Password_Erp
					echo 
					echo "==> What is your postgres password [eg : $erpUser""postgrespwd]"
					read DB_Password_Postgres
					echo 
					#postgres configuration
					# And add ``listen_addresses`` to ``/etc/postgresql/x.x/main/postgresql.conf``
					postgresql_version
					cmd=$(head -n 1 $USER_HOME/commandrun) 
					echo "local	all	postgres	ident" >> /etc/postgresql/$cmd/main/pg_hba.conf
					echo "listen_addresses = '*' ">> /etc/postgresql/$cmd/main/postgresql.conf
					/etc/init.d/postgresql restart
			
					#check psql server
					db_statuscheck
					if [[ "$DB_STATUS" == "UP" ]] ; then
						if [ $Admin_Install == "dev" ]; then
							psql=( psql -v ON_ERROR_STOP=1 )
							postgres='postgres'
							if [ ! -z $DB_Name ]; then
								#"${psql[ $postgres ]}" --username postgres <<-EOSQL
								#	CREATE DATABASE "$DB_Name" ENCODING = 'LATIN1' LC_COLLATE = 'fr_FR' LC_CTYPE = 'fr_FR' CONNECTION LIMIT = 2 ;
								#EOSQL
								#echo
								# Adjust PostgreSQL configuration so that remote connections to the
								# database are possible. 
			
					
								echo 
								echo "*************** Run those instructions in order ********************"
								echo "==> psql"
								echo "==> CREATE USER "$DB_UserName" WITH ENCRYPTED PASSWORD '$DB_Password_Erp';"
								echo "==> ALTER ROLE "$DB_UserName" LOGIN;"
								echo "==> ALTER USER $DB_UserName CREATEUSER CREATEDB REPLICATION CREATEROLE;"
								echo "==> CREATE DATABASE $DB_Name WITH OWNER = $DB_UserName TEMPLATE = template1 CONNECTION LIMIT = 2;"
								echo 
								echo "===== * Check that everything is fine * ====="
								echo "==>to verity user run : \du"
								echo "==>to list databases run : \l"
								echo "==>to exit psql console run : \q"
								echo "==>to exit postgres user run : exit"
								echo 
			
								#pwd for erp connexion
								if [ ! -f /opt/$erpUser/"$erpUser"_user_configuration ]; then
									echo -e "******** $erpUser CONFIGURATION *********\n" > /opt/$erpUser/"$erpUser"_user_configuration
									sudo chown $erpUser:$erpUser /opt/$erpUser/"$erpUser"_user_configuration
								fi
								echo -e "******* DataBase *******\n" >> /opt/$erpUser/"$erpUser"_user_configuration
								echo -e "DB : $DB_Name \nuser : $erpUser \nuserpwd $DB_Password_Erp \nmaster pwd : $DB_Password_Postgres\n" >> /opt/$erpUser/"$erpUser"_user_configuration
								echo -e "== connexion (with local system user) ==\n" >> /opt/$erpUser/"$erpUser"_user_configuration
								echo -e "sudo -i -u $erpUser\n" >> /opt/$erpUser/"$erpUser"_user_configuration
								echo -e "psql $DB_Name\n" >> /opt/$erpUser/"$erpUser"_user_configuration
						
			
								#sudo -u postgres psql
								sudo su - postgres
								#sudo -u postgres psql -c "CREATE DATABASE $DB_Name WITH ENCODING 'UTF8' TEMPLATE template1 CONNECTION LIMIT = 2"
								#sudo su - postgres psql postgres -c "CREATE DATABASE $DB_Name WITH ENCODING 'UTF8' TEMPLATE template0 LC_COLLATE = 'fr_FR' LC_CTYPE = 'fr_FR' CONNECTION LIMIT = 2"
								#sudo -u postgres -H -- psql -d "$DB_Name" -c "$op USER "$DB_UserName" WITH SUPERUSER $DB_Password_Erp"
							fi
						else
							#create sql files
							sudo mkdir $DIR_SqlFiles
							sudo touch $DIR_SqlFiles/command.sql
							#run script
							echo "CREATE USER $DB_UserName WITH ENCRYPTED PASSWORD '$DB_Password_Erp';" > $DIR_SqlFiles/command.sql
							runsqls
							sleep 5
							echo "ALTER ROLE "$DB_UserName" LOGIN;" > $DIR_SqlFiles/command.sql
							runsqls
							sleep 5
							echo "ALTER USER $DB_UserName CREATEUSER CREATEDB REPLICATION CREATEROLE;" > $DIR_SqlFiles/command.sql
							runsqls
							sleep 5
							echo "CREATE DATABASE $DB_Name WITH OWNER = $DB_UserName TEMPLATE = template1 CONNECTION LIMIT = 2;" > $DIR_SqlFiles/command.sql
							runsqls
							sleep 5
							sudo rm -R $DIR_SqlFiles

						fi
					else
						echo "Sorry postgres don't work !!!"
						exit
					fi 
		

					#> amazingerp
					#$ psql
					#> grant all privileges on database erp to erp;
					#erp configuration
					#test file
					echo "create erp-server.conf file or delete it"
			
					#create virtual python env
					#install erp and deps
					#ps : For Ubuntu 14.10, replace virtualenv venv-erpv9 par python /usr/lib/python2.7/dist-packages/virtualenv.py venv-erpv9
					#test file erp/ erp_community/ erp_dev/ venv-erpv/ venv-erpv8/  
					if [ ! -d "/opt/$erpUser/odooconf" ]; then
						create_erp_server_conf
					else
						echo "Delete old erp-server.conf ? [y/n]"
						read deleteerperpserverconf
						if ([ $deleteerperpserverconf == "y" ] || [ "$deleteerperpserverconf" == "Y" ] || [ "$deleteerperpserverconf" == "yes" ] || [ "$deleteerperpserverconf" == "Yes" ] || [ "$deleteerperpserverconf" == "YES" ]); then
							delete_erp_server_conf
							create_erp_server_conf
						fi		
					fi
		
		
					echo "create erp-server.log file or delete it"
			
					#create erp log file
					if [ ! -f "/opt/$erpUser/erp-server.log" ]; then
						create_erp_server_log
					else
						echo "Delete old erp-server.log ? [y/n]"
						read deleteerperpserverlog
						if ([ $deleteerperpserverlog == "y" ] || [ "$deleteerperpserverlog" == "Y" ] || [ "$deleteerperpserverlog" == "yes" ] || [ "$deleteerperpserverlog" == "Yes" ] || [ "$deleteerperpserverlog" == "YES" ]); then
							delete_erp_server_log
							create_erp_server_log
						fi		
					fi
		
					#log repository for supervisor
					if [ ! -d /var/$erpUser ]; then
						sudo mkdir /var/$erpUser
						if [ -d /var/$erpUser ]; then
							if [ ! -d /var/$erpUser/supervisorlog ]; then
								sudo mkdir /var/$erpUser/supervisorlog
								if [ -d /var/$erpUser/supervisorlog ]; then
									if [ ! -f /var/$erpUser/supervisorlog/supervisord-$erpUser-server.log ]; then
										sudo echo "server log !" > /var/$erpUser/supervisorlog/supervisord-$erpUser-server.log
									fi
									if [ ! -f /var/$erpUser/supervisorlog/supervisord-$erpUser-server-longpolling.log ]; then
										sudo echo "server longpolling log !" > /var/$erpUser/supervisorlog/supervisord-$erpUser-server-longpolling.log
									fi
								fi
							fi
						fi
						sudo chown -R $erpUser:$erpUser /var/$erpUser
					else
						sudo chown -R $erpUser:$erpUser /var/$erpUser		
					fi
		
					echo "if [ -f /opt/$erpUser/odoo-server.conf ]; then
					    	echo \"erp-server.conf exists.\"
					    	echo -e \"* Change server config file\"
						sudo sed -i s/\"db_user = .*\"/\"db_user = $DB_UserName\"/g /opt/$DB_UserName/odoo-server.conf
						sudo sed -i s/\"db_name = .*\"/\"db_name = $DB_Name\"/g /opt/$DB_UserName/odoo-server.conf
						sudo sed -i s/\"dbfilter = .*\"/\"dbfilter = ^$DB_UserName\"/g /opt/$DB_UserName/odoo-server.conf
						sudo sed -i s/\"db_password = .*\"/\"db_password = $DB_Password_Erp\"/g /opt/$DB_UserName/odoo-server.conf
						sudo sed -i s/\"admin_passwd = .*\"/\"admin_passwd = $DB_Password_Postgres\"/g /opt/$DB_UserName/odoo-server.conf
			
						if  [ $erpVersion == \"7\" ]; then
							sudo sed -i s/\"addons_path .*\"/\"addons_path = \/opt\/$erpUser\/${tabVersionName[0]}\/openerp\/addons,\/opt\/$erpUser\/erp_community\/addons-enabled,\/opt\/$erpUser\/erp_dev\/addons-enabled\"/g /opt/$erpUser/odoo-server.conf
						else
							sudo sed -i s/\"addons_path .*\"/\"addons_path = \/opt\/$erpUser\/${tabVersionName[0]}\/addons,\/opt\/$erpUser\/erp_community\/addons-enabled,\/opt\/$erpUser\/erp_dev\/addons-enabled\"/g /opt/$erpUser/odoo-server.conf
						fi

						sudo sed -i s/\"; logfile = .*\"/\"logfile = /opt/$erpUser/erp-server.log\"/g /opt/$erpUser/odoo-server.conf
						sudo chmod 600 /opt/$erpUser/odoo-server.conf 

			
						#run supervisor for erp erp-server and erp-server-longpolling
						if [ ! -f /etc/supervisor/conf.d/supervisor-odoo-server.conf ]; then
							clear
							sudo git clone https://gmeyomesse@bitbucket.org/gmeyomesse/supervisor.git /etc/supervisor/conf.d/gitsupervisor
							sudo cp /etc/supervisor/conf.d/gitsupervisor/supervisor-odoo-server-longpolling.conf /etc/supervisor/conf.d/
							sudo cp /etc/supervisor/conf.d/gitsupervisor/supervisor-odoo-server.conf /etc/supervisor/conf.d/
							#remove var gitsupervisor repository
							sudo rm -R /etc/supervisor/conf.d/gitsupervisor
				
						else 
						    echo \"Files supervisor-odoo-server.conf && supervisor-odoo-server-longpolling.conf exist\"
						fi

						#test server and polling files
						if [ -f /etc/supervisor/conf.d/supervisor-odoo-server.conf ]; then
					    		echo \"supervisor-odoo-server.conf exists.\"
							#this cmd bug so replace after
							# sudo sed -i s/\"\[program\:supervisor-erp-server]\"/\"\[program\:supervisor-$erpUser-server]\"/g /etc/supervisor/conf.d/supervisor-odoo-server.conf
				
							#special for v10
							if [ $erpVersion == 10 ]; then
								sed -i s/\"command=.*\"/\"command=\/opt\/$erpUser\/venv-erpv$erpVersion\/bin\/python \/opt\/$erpUser\/${tabVersionName[0]}\/odoo-bin -c \/opt\/$erpUser\/odoo-server.conf --logfile=\/var\/$erpUser\/erp-server.log\"/g /etc/supervisor/conf.d/supervisor-odoo-server.conf
							elif [ $erpVersion == 7 ]; then
								sed -i s/\"command=.*\"/\"command=python \/opt\/$erpUser\/${tabVersionName[0]}\/openerp-server -c \/opt\/$erpUser\/odoo-server.conf --logfile=\/var\/$erpUser\/erp-server.log\"/g /etc/supervisor/conf.d/supervisor-odoo-server.conf				
							else
								sed -i s/\"command=.*\"/\"command=\/opt\/$erpUser\/venv-erpv$erpVersion\/bin\/python \/opt\/$erpUser\/${tabVersionName[0]}\/openerp-server -c \/opt\/$erpUser\/odoo-server.conf --logfile=\/var\/$erpUser\/supervisorlog\/supervisord-${tabVersionName[0]}-server.log\"/g /etc/supervisor/conf.d/supervisor-odoo-server.conf
							fi

							sed -i s/\"user=.*\"/\"user=$erpUser\"/g /etc/supervisor/conf.d/supervisor-odoo-server.conf
							sed -i s/\"directory=.*\"/\"directory=\/opt\/$erpUser\/${tabVersionName[0]}\"/g /etc/supervisor/conf.d/supervisor-odoo-server.conf
							sudo sed -i s/\"environment = .*\"/\"environment = HOME=\\\"\/opt\/$erpUser\/\\\",USER=\\\"$erpUser\\\"\"/g /etc/supervisor/conf.d/supervisor-odoo-server.conf
							#sudo sed -i s/\"logfile=.*\"/\"logfile=\/var\/$erpUser\/supervisorlog\/supervisord-${tabVersionName[0]}-server.log\"/g /etc/supervisor/conf.d/supervisor-odoo-server.conf
						else 
						    echo \"Files supervisor-odoo-server.conf doesn t exists\"
						fi

						if [ -f /etc/supervisor/conf.d/supervisor-odoo-server-longpolling.conf ]; then
					    		echo \"odoo-server.conf doesn t exists.\"
							#this cmd bug so replace after
							#sudo sed -i s/\"\[program\:supervisor-erp-server-longpolling]\"/\"\[program\:supervisor-$erpUser-server-longpolling]\"/g /etc/supervisor/conf.d/supervisor-odoo-server-longpolling.conf
							if [ $erpVersion == 10 ]; then
								sed -i s/\"command=.*\"/\"command=\/opt\/$erpUser\/venv-erpv$erpVersion\/bin\/python \/opt\/$erpUser\/${tabVersionName[0]}\/odoo-bin -c \/opt\/$erpUser\/odoo-server.conf --logfile=\/opt\/$erpUser\/erp-server-longpolling.log\"/g /etc/supervisor/conf.d/supervisor-odoo-server-longpolling.conf
							elif [ $erpVersion == 7 ]; then
								sed -i s/\"command=.*\"/\"command=python \/opt\/$erpUser\/${tabVersionName[0]}\/openerp-server -c \/opt\/$erpUser\/odoo-server.conf --logfile=\/var\/$erpUser\/erp-server.log\"/g /etc/supervisor/conf.d/supervisor-odoo-server.conf								
							else
								sed -i s/\"command=.*\"/\"command=\/opt\/$erpUser\/venv-erpv$erpVersion\/bin\/python \/opt\/$erpUser\/${tabVersionName[0]}\/openerp-gevent -c \/opt\/$erpUser\/odoo-server.conf --logfile=\/var\/$erpUser\/supervisorlog\/supervisord-erp-server-longpolling.log\"/g /etc/supervisor/conf.d/supervisor-odoo-server-longpolling.conf
							fi

							sed -i s/\"user=.*\"/\"user=$erpUser\"/g /etc/supervisor/conf.d/supervisor-odoo-server-longpolling.conf
							sed -i s/\"directory=.*\"/\"directory=\/opt\/$erpUser\/${tabVersionName[0]}\"/g /etc/supervisor/conf.d/supervisor-odoo-server-longpolling.conf
							sudo sed -i s/\"environment = .*\"/\"environment = HOME=\\\"\/opt\/$erpUser\/\\\",USER=\\\"$erpUser\\\"\"/g /etc/supervisor/conf.d/supervisor-odoo-server-longpolling.conf
							#sudo sed -i s/\"logfile=.*\"/\"logfile=\/var\/$erpUser\/supervisorlog\/supervisord-erp-server-longpolling.log\"/g /etc/supervisor/conf.d/supervisor-odoo-server-longpolling.conf

						else 
						    echo \"Files erp-server-longpolling doesn't exists\"
						fi

		
					else 
					    echo \"File erp-server-conf doesn't exist\"
					    #exec service ssh restart
					    #/usr/sbin/sshd -D
					fi" > $USER_HOME/commandrun
					cmd=$(head -n 100 $USER_HOME/commandrun)
					sudo -H -u root /bin/bash -c "$cmd"


					configure_server_supervisor "longpolling"
					configure_server_supervisor "server"


					#Nginx
					#put erp derrière un Reverse Proxy HTTP.
					apt-get remove apache2 -y
					echo "create erp-server.log file or delete it"
			
					#create erp log file
					if [ ! -d /etc/nginx/sites-available/clonenginx ]; then
						create_nginx
					else
						echo "Delete old nginx ? [y/n]"
						read deleteoldnginx
						if ([ $deleteoldnginx == "y" ] || [ "$deleteoldnginx" == "Y" ] || [ "$deleteoldnginx" == "yes" ] || [ "$deleteoldnginx" == "Yes" ] || [ "$deleteoldnginx" == "YES" ]); then
							delete_nginx
							create_nginx
						fi		
					fi
		
		
					#change config nginx port
					echo "Change proxy port ? [default: 8069]"
					read proxyPort
					while true; do
						if [ ! -z "$proxyPort" ] ; then
							if ! [[ $proxyPort =~ $re ]] ; then
							   	echo "Error ! Your port is not correct."
								echo " You cant attibute port like 8080 or 80xx (xx is number)"
						
							else
								echo "sed -i s/\"proxy_pass http\:\\/\\/127.0.0.1:8069\"/\"proxy_pass http\:\\/\\/127.0.0.1:$proxyPort\"/g /etc/nginx/sites-available/nginx-$erpUser.conf" > $USER_HOME/commandrun
							  	cmd=$(head -n 100 $USER_HOME/commandrun)
								sudo -H -u root /bin/bash -c "$cmd" 
								sudo echo "xmlrpc_port = $proxyPort" >> /opt/$erpUser/odoo-server.conf
								break
							fi
						fi
						echo "Change proxy port ? [default: 8069]"
						read proxyPort
					done

			
					if [ $erpVersion != "7" ]; then
						echo "Change proxy longpolling port ? [default: 8072]"
						read proxyPortLongpolling
						while true; do
							if [ ! -z "$proxyPortLongpolling" ] ; then
								if ! [[ $proxyPortLongpolling =~ $re ]] ; then
								   	echo "Error ! Your port is not correct."
									echo " You cant attibute port like 8080 or 80xx (xx is number)"
								else
								  	echo "sed -i s/\"proxy_pass http\:\\/\\/127.0.0.1:8072\"/\"proxy_pass http\:\\/\\/127.0.0.1:$proxyPortLongpolling\"/g /etc/nginx/sites-available/nginx-$erpUser.conf" > $USER_HOME/commandrun
									cmd=$(head -n 100 $USER_HOME/commandrun)
									sudo -H -u root /bin/bash -c "$cmd"
									sudo echo "longpolling_port = $proxyPortLongpolling" >> /opt/$erpUser/odoo-server.conf
									break
								fi
							fi
							echo "Change proxy longpolling port ? [default: 8072]"
							read proxyPortLongpolling
						done
						#add to config file
						echo -e "\n\n ******** PORT NGINX ACCESS ****** \n odoo-server : $proxyPort \n odoo-server-longpolling : $proxyPortLongpolling \n" >> /opt/$erpUser/"$erpUser"_user_configuration
					else
						#add to config file
						echo -e "\n\n ******** PORT NGINX ACCESS ****** \n odoo-server : $proxyPort \n" >> /opt/$erpUser/"$erpUser"_user_configuration
					fi

					#change access (important for openErp)
					if [ -f /opt/$erpUser/odoo-server.conf ]; then
						sudo chmod a+rwx /opt/$erpUser/odoo-server.conf
					fi


					#delete default
					if [ -h /etc/nginx/sites-enabled/default ];then
						sudo rm /etc/nginx/sites-enabled/default
					fi
					#restart nginx	
					sudo systemctl stop nginx
					sudo systemctl start nginx
					sudo systemctl reload nginx
					sudo systemctl restart nginx 
					#or sudo /etc/init.d/nginx restart
					
					

					#write config & show it for dev mode
					data_config
					if [ $Admin_Install == "dev" ]; then
						less /opt/$erpUser/"$erpUser"_user_configuration
					fi
			
					#dev tools
					if [ $Admin_Install == "dev" ]; then
						clear
						echo 
						echo "Do you want tools for dev (pycharm community) [y/n]:"
						read response
						if ([ $response == "y" ] || [ "$response" == "Y" ] || [ "$response" == "yes" ] || [ "$response" == "Yes" ] || [ "$response" == "YES" ]); then
							sudo add-apt-repository ppa:mystic-mirage/pycharm
							sudo apt update
							#sudo apt install pycharm
							#for the community edition
							sudo apt-get -y install pycharm-community
							#sudo apt remove pycharm pycharm-community
							echo 
							echo "******** Run : $ pycharm-community *********"
							echo

							#clone git remotes
							clone_git_remote
						fi


						clear
						echo
						echo "Do you want pgadmin ? [y/n]"
						read response
						if ([ $response == "y" ] || [ "$response" == "Y" ] || [ "$response" == "yes" ] || [ "$response" == "Yes" ] || [ "$response" == "YES" ]); then
							pgadmin_installed
							if [ -s $USER_HOME/commandrun ]; then
								sudo apt-get -y install pgadmin3
							fi
						fi

			

						clear
						ungit_installed
						if [ ! -s $USER_HOME/commandrun ]; then
							clear
							echo 
							echo "Do you want tools for dev (ungit) [y/n]:"
							read response
							if ([ $response == "y" ] || [ "$response" == "Y" ] || [ "$response" == "yes" ] || [ "$response" == "Yes" ] || [ "$response" == "YES" ]); then
						
								#first install nodejs	
								#don't use ubuntu repo for install nodejs
								# https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions		
								sudo curl -sL https://deb.nodesource.com/setup_7.x | sudo -E bash -
								sudo apt-get install -y nodejs
								#important configure nodejs for run it
								if [ -L /usr/local/bin/node ]; then
								  	echo "=> File doesn't exist"
								    	sudo rm /usr/local/bin/node
									sudo rm /usr/local/bin/npm
								fi
			
								#install ungit
								sudo npm install -g ungit
								#after install this create 2 symb links
								#/usr/local/bin/ungit -> /usr/local/lib/node_modules/ungit/bin/ungit
								#/usr/local/bin/0ungit-credentials-helper -> /usr/local/lib/node_modules/ungit/bin/credentials-helper
								#you have node module on /usr/local/lib (librairies locales node_modules/python2.7/python3.5)
			
								echo 
								echo "******** Run : $ ungit *********"
								echo

								#add which_term function on .bashrc
								copy_content_to_bashrc 	

								echo 
								echo "Do you want to run ungit [y/n]:"
								read response
								if ([ $response == "y" ] || [ "$response" == "Y" ] || [ "$response" == "yes" ] || [ "$response" == "Yes" ] || [ "$response" == "YES" ]); then
									echo
									echo "wait..."
									run_ungit
								else
									echo
									echo "wait..."
									automatic_supervisor
									run_browser
								fi
							else
								echo
								echo "wait..."
								automatic_supervisor
								echo 
								echo "Do you want to run erp on browser ? [yes/no]"
								read response
								echo
								if ([ $response == "y" ] || [ "$response" == "Y" ] || [ "$response" == "yes" ] || [ "$response" == "Yes" ] || [ "$response" == "YES" ]); then
									run_browser
								fi
							fi
				
						else
							echo 
							echo "Do you want to run ungit [y/n]:"
							read response
							if ([ $response == "y" ] || [ "$response" == "Y" ] || [ "$response" == "yes" ] || [ "$response" == "Yes" ] || [ "$response" == "YES" ]); then
								echo
								echo "wait..."
								run_ungit
							else
								echo
								echo "wait..."
								automatic_supervisor
								echo 
								echo "Do you want to run erp on browser ? [yes/no]"
								read response
								echo
								if ([ $response == "y" ] || [ "$response" == "Y" ] || [ "$response" == "yes" ] || [ "$response" == "Yes" ] || [ "$response" == "YES" ]); then
									run_browser
								fi
							fi
						fi
						ask_to_dump_db
						ask_to_delete_db
					else
						echo
						echo "wait..."
						automatic_supervisor
						echo 
						echo "Do you want to run erp on browser ? [yes/no]"
						read response
						echo
						if ([ $response == "y" ] || [ "$response" == "Y" ] || [ "$response" == "yes" ] || [ "$response" == "Yes" ] || [ "$response" == "YES" ]); then
							ask_to_dump_db
							ask_to_delete_db
							run_browser
						fi
					fi
					break
				else
				  echo "Version selected is not availabled."
				fi
				echo
				echo "OpenERP version available to install [7,8,9 or 10]"
				echo "You must choose one of these version or CTRL + C to exit!"
				read erpVersion
			done
		else
			echo "Cannot create 2 users with same name"
		fi
		break

	else
		if ([ $response != "n" ] && [ "$response" != "N" ] && [ "$response" != "no" ] && [ "$response" != "NO" ] && [ "$response" != "NO" ]); then
		  	echo "Sorry i don't understand what you want^^"
			echo "All I understand is yes or no"
			echo "(°-°)"
		else
			break
		fi
	fi
	echo "Do you want to continue [yes/no]:"
	read response
done
