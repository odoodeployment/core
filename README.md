# OpenErp Instances Deployment
This project contains essentially a bash script to deploy multiple server instances OpenERP easily on a single computer.
The advantage is that you can have Openerp v7, v8 Odoo, Odoo v9 that running simultaneously on the same computer.

It incorporates tools :

- Nginx
- PostgreSQL 9.3
- OpenERP
- GIT

During the deployment of an ERP instance, you have demonstration datas that are loaded automatically on your postgres database.
You also have the option to deploy a local development environment by installing PyCharm in its community version and pgAdmin.
During installation, you can add git repositories that will be integreted on your development project environment into special home directory (/opt/your_project/erp_dev/your_repo)
After complete installation run on your browser this uri : localhost:selected_port
This script has been tested on Ubuntu 14.04 LTS and therefore still in beta.

Our future improvement:

- improved bash script
- code factorization
- nginx best configuration
- web interface development 